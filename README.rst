===========================
ADE Development Environment
===========================

The ADE Development Environment (ADE) uses docker and gitlab to
manage environments of per project development tools and optional
volume images. Volume images may contain additional development tools
or released software versions. It enables easy switching of branches
for all images.

For a public project using ADE as an example, see `Autoware.Auto
<https://gitlab.com/AutowareAuto/AutowareAuto>`_.


Install ade
-----------

Run the following command in a terminal:

`curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-ade.sh | bash`