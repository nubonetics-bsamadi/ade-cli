.. ADE documentation master file, created by
   sphinx-quickstart on Sat Oct 13 18:40:41 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ADE Development Environment
===========================

The ADE Development Environment (ADE -  *pronounced [ey]-[dee]-[ee]*) is a modular `Docker-based`_ tool to ensure
that all developers in a project have a **common, consistent development environment**. 

With ADE developers:

1. Are confident that what works on one of their computers will also work on all ADE-enabled computers
2. Can comfortably install new packages and dependencies, knowing that the environment can easily be
   returned to its original state
3. Can mount different volumes (which can be standalone libraries, IDEs, and other tools) and easily
   switch between versions of these volumes without affecting other parts of the environment

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   intro
   install
   setup
   usage
   CHANGES



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. _Docker-based: https://docs.docker.com/
